import React from 'react';
import "./Square.css";

const Square = (props) => {
  const handleClick = (e) => {
    e.preventDefault();

    props.handleSquareClick(e, props.i, props.j);
  }

  return (
    <div className={'square square--' + props.status} data-i={props.i} data-j={props.j} 
      onClick={handleClick}
      onContextMenu={handleClick}
    ></div>
  );
};

export default Square;

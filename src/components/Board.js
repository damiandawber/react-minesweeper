import React, { Component } from 'react';
import config from '../config';
import Square from './Square';
import { scan, initialiseMines, initialiseBoard, isMine, mineAllUserBoard, isBoardAWinner } from '../lib/minesweeper';
import './Board.css';

class Board extends Component {

  /**
   * @constructor
   */
  constructor(props) {
    super(props);

    const initialBoard = initialiseBoard(config.numRows, config.numCols);

    this.state = {
      gameOver: false,
      isWinner: false,
      mines: [],
      userBoard: initialBoard,
    };

    this.handleSquareClick = this.handleSquareClick.bind(this);
  }

  createSquares() {
    const squares = [];
    
    for(let i = 0; i < config.numRows; i+=1) {
      squares[i] = [];

      for(let j = 0; j < config.numCols; j+=1) {
        squares[i][j] = <Square 
          handleSquareClick={this.handleSquareClick}
          key={i.toString() + j.toString()} i={i} j={j} status={this.state.userBoard[i][j]} />;  
      }
    }

    return squares;
  }

  handleSquareClick(evt, i, j) {
    if(this.state.gameOver) {
      return;
    }

    const clickType = evt.type;

    if(['c'].indexOf(this.state.userBoard[i][j]) > -1) {
      // You can't click cleared squares
      return;
    }

    let mines = this.state.mines;

    if(clickType === 'contextmenu') {
      const userBoard = this.state.userBoard;
      userBoard[i][j] = userBoard[i][j] === 'f' ? 'n' : 'f';

      this.setState({
        userBoard: userBoard
      });
    } else {

      if(this.state.mines.length === 0) {
        // The first time a user clicks a square is when we actually generate mines
        mines = initialiseMines(config.numMines, config.numRows, config.numCols, i, j);
        
        this.setState({
          mines: mines
        });
      }

      if(isMine(mines, i, j)) {
        const userBoard = mineAllUserBoard(mines, this.state.userBoard);

        userBoard[i][j] = 'mc';

        this.setState({
          userBoard: userBoard,
          gameOver: true
        });

        console.log('Game Over! Mined');
      } else {
        const userBoard = scan(mines, this.state.userBoard, i, j);
        const minedUserBoard = mineAllUserBoard(mines, userBoard);
        const isWinner = isBoardAWinner(minedUserBoard);

        this.setState({
          userBoard: isWinner ? minedUserBoard : userBoard,
          isWinner: isWinner,
          gameOver: isWinner,
        });
      }
    }
  }

  render() {
    return (
      <div className={'board board--' + (this.state.gameOver ? 'cabbaged' : 'active')}>
        <p className="board__status">{this.state.gameOver ? (this.state.isWinner ? '🎉' : '😭') : '😊'}</p>
        <div className="board__grid">
          {
            this.createSquares().map((s, idx) => (
              <div className="board__grid-row" key={idx} data-row={idx} >
                {s}
              </div>
            ))
          }    
        </div>
      </div>
    );
  }
}

export default Board;

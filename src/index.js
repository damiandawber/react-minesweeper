import React from 'react';
import ReactDOM from 'react-dom';
import Board from './components/Board';
import './index.css';

ReactDOM.render(
  (<div>
    <h1>React Minesweeper</h1>
    <Board />
    <p><a href="https://bitbucket.org/damiandawber/react-minesweeper/">by DD</a></p>
  </div>),
  document.getElementById('root')
);

import { deepCopy } from './utils';

/**
 * Initialise mines 2-D array.
 *
 * Note that we must not place a mine on the first chosen square
 * or any of its neighbours
 *
 * This is why you never lose at Minesweeper on your first go!
 *
 * @param {number} numMines Number of mines
 * @param {number} numRows Number of rows in grid
 * @param {number} numCols Number of cols in grid
 * @param {number} chosenSquareRow Row index of chosen square (which we ignore)
 * @param {number} chosenSquareCol Col index of chosen square (which we ignore)
 * @return {Array}
 *
 * @todo Ensure mine indices not in region of chosen square
 */
const initialiseMines = (numMines, numRows, numCols, chosenSquareRow, chosenSquareCol) => {
  const numSquares = numRows * numCols;

  if(numMines >= numSquares) {
    // Don't be giving me gip
    numMines = numSquares - 1;
  }

  // Randomly assign mines
  const availableMineSlots = (new Array(numSquares)).fill(0).map((elm, idx) => idx);

  // Mine indices should not be in region of chosen square
  const chunkedSlots = [];
  for (let i = 0; i < availableMineSlots.length; i += numCols) {
    chunkedSlots.push(availableMineSlots.slice(i,i+numCols));
  }
  for(let i = chosenSquareRow - 1; i <= chosenSquareRow + 1; i += 1) {
    for(let j = chosenSquareCol - 1; j <= chosenSquareCol + 1; j += 1) {
      if(typeof (chunkedSlots[i] || [])[j] !== 'undefined') {
        availableMineSlots.splice(availableMineSlots.indexOf(j + (i * numCols)), 1);
      }
    }
  }

  // Assign mines
  let mines = (new Array(numSquares)).fill(0);
  for(let i = 0; i < numMines; i++) {
    const randomIndex = Math.floor(Math.random() * availableMineSlots.length);

    mines[availableMineSlots[randomIndex]] = 1;

    availableMineSlots.splice(randomIndex, 1);
  }

  // chunk back to 2-d
  const chunkedMines = [];
  for (let i = 0; i < mines.length; i += numCols) {
    chunkedMines.push(mines.slice(i,i+numCols));
  }


  return chunkedMines;
};

/**
 * Helper initialise user board with 'n' strings
 * 'n' = neutral, unplayed
 * 'f' = flagged by user
 * 'c' = cleared
 * 'm' = mined
 *
 * @param {number} numRows Number of rows in grid
 * @param {number} numCols Number of cols in grid
 * @return {Array}
 */
const initialiseBoard = (numRows, numCols) => {
  const board = [];

  let i = 0;
  while(i < numRows) {
    board.push((new Array(numCols)).fill('n'));
    i += 1;
  }

  return board;
};

/**
 * Helper - is square a mine?
 * @param {Array} mines 2-D array containing bool-esque values representing mines at i,j
 * @param {number} i Row number of chosen square
 * @param {number} j Column number of chosen square
 * @return {Boolean}
 */
const isMine = (mines, i,j) => !!(mines[i] || [])[j];

/**
 * Count num mines in surrounding 8 neighbours
 * @param {number} i Row number of chosen square
 * @param {number} j Column number of chosen square
 * @return {number}
 */
const countSurroundingMines = (mines, i, j) => {
  let numMinesFound = 0;
  
  for(let m = i - 1; m <= i+1; m+=1) {
    for(let n = j - 1; n <= j+1; n+=1) {
      let hit = isMine(mines, m,n);
      if(hit) {
        numMinesFound += 1;
      }
    }
  }
  
  return numMinesFound;
};

/**
 * Helper scan surrounding mines iteratively and return a copy of the user board
 * @param {Array} mines
 * @param {Array} userBoard 2-D array reference to current user grid
 * @param {number} i Row number
 * @param {number} j Column number
 * @return {Array}
 */
const scan = (mines, userBoard, i, j) => {
  let board = deepCopy(userBoard);

  if(board[i][j] !== 'n') {
    // Prevent re-checking checked items
    return board;
  }
  
  var cnt = countSurroundingMines(mines, i, j);

  if(cnt) {
    board[i][j] = cnt;
  } else {
    board[i][j] = 'c';
    
    // Recursively scan all surrounding, and keep going when no mines 
    // are found in the surrounding 8 neighbours
    for(var m = i - 1; m <= i+1; m+=1) {
      for(var n = j - 1; n <= j+1; n+=1) {
        if(typeof (board[m] || [])[n] !== 'undefined') {
          board = scan(mines, board, m, n);
        }
      }
    }
  }

  return board;
};

/**
 * Update userboard to show all mines (game over)
 */
const mineAllUserBoard = (mines, userBoard) => {
  let board = deepCopy(userBoard);

  for(let i = 0, ii = mines.length; i < ii; i += 1) {
    const mineRow = mines[i];
    for(let j = 0, jj = mineRow.length; j < jj; j += 1) {
      const mineCheck = !!mineRow[j];

      if(mineCheck) {
        board[i][j] = 'm';
      }
    }
  }

  return board;
};

/**
 * Check for winner, if the board is cleared or flagged
 * and merged with possible mines then we have a winner
 */
const isBoardAWinner = (minedUserBoard) => {
  let isWinner = true;

  for(let i = 0, ii = minedUserBoard.length; i < ii; i += 1) {
    const minedRow = minedUserBoard[i];
    for(let j = 0, jj = minedRow.length; j < jj; j += 1) {
      const check = minedRow[j] === 'n';
      if(check) {
        isWinner = false;
      }
    }
  }

  return isWinner;
};

export {isMine, initialiseMines, initialiseBoard, countSurroundingMines, scan, mineAllUserBoard, isBoardAWinner};

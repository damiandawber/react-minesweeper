/**
 * Deep copy array
 * @param {Array} arr
 */
const deepCopy = (arr) => {
  return JSON.parse(JSON.stringify(arr));
}

export {deepCopy};
